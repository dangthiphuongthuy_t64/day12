<!DOCTYPE html>
<html lang='en'>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>

    <title>Register</title>
</head>

<body>
    <?php
    session_start();

    if (!empty($_POST['register'])) {
        header("Location: ./list.php ");
    }
    ?>
    <form method="post" action="" enctype="multipart/form-data" action=?#?>
        <fieldset class="register-form">
            <div class="form">
                <div class="title">
                    <div class="input-text">
                        Bạn đã đăng ký thành công sinh viên
                    </div>
                </div>


                <input type='submit' class="button" name="register" value='Quay lại danh sách sinh viên' />
            </div>

        </fieldset>
    </form>
</body>

</html>