<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
</head>

<body>
    <?php
    session_start();

    if (!empty($_POST['add'])) {
        header("Location: ./register.php ");
    }
    ?>
    <form id="myFormConfirm" method="post" action="" enctype="multipart/form-data" action=?#?>
        <fieldset class="register-form">
            <div class="form">

                <div class="title">
                    <div class="input-text">
                        Khoa</div>
                    <div>
                        <?php echo $_SESSION['department']; ?>
                    </div>
                </div>

                <div class="title">
                    <div class="input-text">
                        Từ khóa</div>
                    <div>
                        <?php echo $_SESSION['note']; ?>
                    </div>
                </div>


                <input type="button" class="button" onclick="myFunction()" value="Xóa">
                <input type='submit' class="button" name="search" value='Tìm kiếm' />
            </div>

            <div>
                Số sinh viên tìm thấy: XXX
                <input type='submit' class="button" name="add" value='Thêm' />
            </div>

            <table align="center" cellspacing="0" cellpadding="1" width="650px">
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>

                <tr>
                    <th>1</th>
                    <th>Nguyễn Văn A</th>
                    <th>Khoa học máy tính</th>
                    <th>
                        <input type='submit' class="button" name="delete" value='Xóa' />
                    </th>
                    <th>
                        <input type='submit' class="button" name="update" value='Sửa' />
                    </th>
                </tr>

                <tr>
                    <th>2</th>
                    <th>Trần Thị B</th>
                    <th>Khoa học máy tính</th>
                    <th>
                        <input type='submit' class="button" name="delete" value='Xóa' />
                    </th>
                    <th>
                        <input type='submit' class="button" name="update" value='Sửa' />
                    </th>
                </tr>

                <tr>
                    <th>3</th>
                    <th>Nguyễn Hoàng C</th>
                    <th>Khoa học vật liệu</th>
                    <th>
                        <input type='submit' class="button" name="delete" value='Xóa' />
                    </th>
                    <th>
                        <input type='submit' class="button" name="update" value='Sửa' />
                    </th>
                </tr>

                <tr>
                    <th>4</th>
                    <th>Đinh Quang D</th>
                    <th>Khoa học vật liệu</th>
                    <th>
                        <input type='submit' class="button" name="delete" value='Xóa' />
                    </th>
                    <th>
                        <input type='submit' class="button" name="update" value='Sửa' />
                    </th>
                </tr>

            </table>


        </fieldset>
    </form>

    <script>
        function myFunction() {
            document.getElementById("myForm").reset();
        }
    </script>
</body>

</html>